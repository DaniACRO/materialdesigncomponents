package cat.itb.mdc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements  View.OnClickListener {
    private Button loginBTN;
    private Button registrerBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginBTN = findViewById(R.id.loginBTN);
        registrerBTN = findViewById(R.id.registerBTN);

        loginBTN.setOnClickListener(this);
        registrerBTN.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loginBTN:
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                break;
            case R.id.registerBTN:
                Intent i2 = new Intent(MainActivity.this, RegisterAcitivty.class);
                startActivity(i2);
                break;
        }
    }
}