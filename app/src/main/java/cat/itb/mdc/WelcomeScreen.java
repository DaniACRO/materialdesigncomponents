package cat.itb.mdc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class WelcomeScreen extends AppCompatActivity {
    private TextView welcomeMSG;
    private String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null){
            userName = bundle.getString("name");
        }

        welcomeMSG = findViewById(R.id.textView);

        String msg = getString(R.string.welcomeMSG);
        welcomeMSG.setText(userName +", " + msg);
    }
}