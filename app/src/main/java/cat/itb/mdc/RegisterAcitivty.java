package cat.itb.mdc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class RegisterAcitivty extends AppCompatActivity implements View.OnClickListener {
    private TextInputLayout userInput;
    private TextInputLayout passInput;
    private TextInputLayout repeatPassInput;
    private TextInputLayout emailInput;
    private TextInputLayout nameInput;
    private TextInputLayout surnamesInput;
    private TextInputLayout birthInput;
    private TextInputLayout genderInput;

    private TextInputEditText userEditText;
    private TextInputEditText passEditText;
    private TextInputEditText repeatPassEditText;
    private TextInputEditText emailEditText;
    private TextInputEditText nameEditText;
    private TextInputEditText surnamesEditText;
    private TextInputEditText birthEditText;

    private MaterialCheckBox accept;

    private Button loginBTN;
    private Button registerBTN;

    private String[] generos = {"Male", "Female", "Other"};
    //
    private AutoCompleteTextView genderDropDown;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_acitivty);

        userInput = findViewById(R.id.userInput);
        passInput = findViewById(R.id.passInput);
        repeatPassInput = findViewById(R.id.repeatInput);
        emailInput = findViewById(R.id.emailInput);
        nameInput = findViewById(R.id.nameInput);
        surnamesInput = findViewById(R.id.surnamesInput);
        birthInput = findViewById(R.id.birthInput);
        genderInput = findViewById(R.id.genderInput);

        userEditText = findViewById(R.id.userEditText);
        passEditText = findViewById(R.id.passEditText);
        repeatPassEditText = findViewById(R.id.repeatEditText);
        emailEditText = findViewById(R.id.emailEditText);
        nameEditText = findViewById(R.id.nameEditText);
        surnamesEditText = findViewById(R.id.surnamesEditText);
        birthEditText = findViewById(R.id.birthEditText);

        accept = findViewById(R.id.acceptCheckBox);

        genderDropDown = findViewById(R.id.autoComplete);

        registerBTN = findViewById(R.id.registerBTN);
        loginBTN = findViewById(R.id.loginBTN);

        accept.setOnClickListener(this);
        registerBTN.setOnClickListener(this);
        loginBTN.setOnClickListener(this);
        birthEditText.setOnClickListener(this);
        birthEditText.setKeyListener(null);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegisterAcitivty.this, R.layout.drop_down_gender_items, generos);
        genderDropDown.setAdapter(adapter);

        userEditText.addTextChangedListener(new AddListenerOnTextChange(this, userInput));
        passEditText.addTextChangedListener(new AddListenerOnTextChange(this, passInput));
        repeatPassEditText.addTextChangedListener(new AddListenerOnTextChange(this, repeatPassInput));
        emailEditText.addTextChangedListener(new AddListenerOnTextChange(this, emailInput));
        nameEditText.addTextChangedListener(new AddListenerOnTextChange(this, nameInput));
        surnamesEditText.addTextChangedListener(new AddListenerOnTextChange(this, surnamesInput));
        birthEditText.addTextChangedListener(new AddListenerOnTextChange(this, birthInput));
        genderDropDown.addTextChangedListener(new AddListenerOnTextChange(this, genderInput));


    }

    public void showDialog() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(RegisterAcitivty.this);
        builder.setTitle(R.string.dialogTitle);
        builder.setMessage(R.string.dialogMsg);
        builder.setPositiveButton(R.string.login, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(getApplicationContext(), WelcomeScreen.class);
                i.putExtra("name", userEditText.getText().toString());
                startActivity(i);
            }
        });
        builder.show();
    }

    public void datePicker(String msg) {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText(msg);
        final MaterialDatePicker<Long> picker = builder.build();
        picker.show(getSupportFragmentManager(), picker.toString());
        if (picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
            @Override
            public void onPositiveButtonClick(Long selection) {
                birthEditText.setText(String.valueOf(picker.getHeaderText()));
            }
        })) ;
    }

    public void isEmpty(TextInputLayout t){
        String msg = getString(R.string.required);
        t.setError(msg);

    }

    @Override
    public void onClick(View v) {
        String msg;
        switch (v.getId()) {
            case R.id.registerBTN:

                if (userEditText.getText().toString().isEmpty() ||
                        passEditText.getText().length() < 8 ||
                        !passEditText.getText().toString().equals(repeatPassEditText.getText().toString()) ||
                        emailEditText.getText().toString().isEmpty() ||
                        nameEditText.getText().toString().isEmpty() ||
                        surnamesEditText.getText().toString().isEmpty() ||
                        birthEditText.getText().toString().isEmpty() ||
                        genderDropDown.getText().toString().isEmpty() ||
                        !accept.isChecked()) {

                    if (userEditText.getText().toString().isEmpty()) {
                        isEmpty(userInput);
                    }
                    if (passEditText.getText().length() < 8) {
                        msg = getString(R.string.min);
                        passInput.setError(msg);
                    }
                    if (!passEditText.getText().toString().equals(repeatPassEditText.getText().toString())) {
                        msg = getString(R.string.notequal);
                        repeatPassInput.setError(msg);
                    }
                    if (emailEditText.getText().toString().isEmpty()) {
                        isEmpty(emailInput);
                    }
                    if (nameEditText.getText().toString().isEmpty()) {
                        isEmpty(nameInput);
                    }
                    if (surnamesEditText.getText().toString().isEmpty()) {
                        isEmpty(surnamesInput);
                    }
                    if (birthEditText.getText().toString().isEmpty()) {
                        isEmpty(birthInput);
                    }
                    if (genderDropDown.getText().toString().isEmpty()) {
                        isEmpty(genderInput);
                    }
                    if (!accept.isChecked()) {
                        accept.setTextColor(getResources().getColor(R.color.accetpnotchecket));
                    }
                } else {
                    showDialog();
                }
                break;

            case R.id.birthEditText:
                msg = getString(R.string.dateTitle);
                datePicker(msg);
                break;

            case R.id.acceptCheckBox:
                accept.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;

            case R.id.loginBTN:
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                break;

        }
    }
}