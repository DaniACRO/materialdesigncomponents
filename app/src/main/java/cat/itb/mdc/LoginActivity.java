package cat.itb.mdc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private TextInputLayout userInput;
    private TextInputLayout passInput;
    private TextInputEditText userEditText;
    private TextInputEditText passEditText;
    private Button loginBTN;
    private Button registerBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userInput = findViewById(R.id.userInput);
        passInput = findViewById(R.id.passInput);

        userEditText = findViewById(R.id.userEditText);
        passEditText = findViewById(R.id.passwordEditText);


        loginBTN = findViewById(R.id.loginBTN);
        registerBTN = findViewById(R.id.registerBTN);

        loginBTN.setOnClickListener(this);
        registerBTN.setOnClickListener(this);

        passEditText.addTextChangedListener(new AddListenerOnTextChange(this, passInput));
        userEditText.addTextChangedListener(new AddListenerOnTextChange(this, userInput));
    }

    @Override
    public void onClick(View v) {
        String msg;
        switch (v.getId()) {
            case R.id.loginBTN:
                if (userEditText.getText().toString().isEmpty() || passEditText.getText().length() < 8){
                    if (passEditText.getText().length() < 8){
                        msg = getString(R.string.min);
                        passInput.setError(msg);
                    }
                    if (userEditText.getText().toString().isEmpty()){
                        msg = getString(R.string.required);
                        userInput.setError(msg);
                    }
                }else{
                    Intent i = new Intent(getApplicationContext(), WelcomeScreen.class);
                    i.putExtra("name", userEditText.getText().toString());
                    startActivity(i);
                }
                break;

            case R.id.registerBTN:
                Intent i2 = new Intent(getApplicationContext(), RegisterAcitivty.class);
                startActivity(i2);
        }

    }
}